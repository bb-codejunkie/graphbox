﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GraphBox
{
    internal static class IEnumerableExtensions
    {
        public static void Each<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            foreach (T item in enumeration)
            {
                action(item);
            }
        }
    }
}
