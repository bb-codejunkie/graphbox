﻿using GraphBoxMeasurement.Dtos;
using System;
using System.Collections.Generic;

namespace GraphBoxMeasurement.StatisticsHandlers
{
    public interface IStatisticsHandler
    {
        void Handle(Guid sessionId, IEnumerable<IStatisticsDto> statisticsDtos);
    }
}
