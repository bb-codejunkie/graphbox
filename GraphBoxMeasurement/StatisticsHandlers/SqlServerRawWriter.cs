﻿using GraphBoxMeasurement.Database;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphBox;

namespace GraphBoxMeasurement.StatisticsHandlers
{
    public class SqlServerRawWriter : IStatisticsHandler
    {
        private readonly ICreateTableIfNotExsists _createTableIfNotExsists;
        private readonly SqlConnection _connection;

        private readonly string createSql = "CREATE TABLE [dbo].[%%table%%]( " +
                                            "[SessionId] [uniqueidentifier] NOT NULL,  " +
                                            "[ElapsedMilliseconds] [bigint] NOT NULL " +
                                            ") ON [PRIMARY]";


        public SqlServerRawWriter(ISqlServerConnection sqlServerConnection, ICreateTableIfNotExsists createTableIfNotExsists)
        {
            _connection = sqlServerConnection.Connection;
            _createTableIfNotExsists = createTableIfNotExsists;
        }

        public void Handle(Guid sessionId, IEnumerable<Dtos.IStatisticsDto> statisticsDtos)
        {
            statisticsDtos.Select(dto => dto.Name)
                .Distinct()
                .Each(name => _createTableIfNotExsists.Execute(name, createSql.Replace("%%table%%", name)));

            _connection.Open();

            Console.Write("writing to {0} on SQL Server: ", _connection.Database);

            statisticsDtos.Each(dto =>
            {
                var sqlCommand = new SqlCommand("INSERT INTO [dbo].[" + dto.Name + "] (SessionId, ElapsedMilliseconds) VALUES (@SessionId, @ElapsedMilliseconds)", _connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SessionId", sessionId));
                sqlCommand.Parameters.Add(new SqlParameter("@ElapsedMilliseconds", dto.ElapsedMilliseconds));
                sqlCommand.ExecuteNonQuery();
                Console.Write(".");
            });

            Console.Write("\n");
            _connection.Close();
        }
    }
}
