﻿using GraphBox;
using GraphBoxMeasurement.Dtos;
using System;
using System.Linq;
using System.Collections.Generic;

namespace GraphBoxMeasurement.StatisticsHandlers
{
    public class AvgConsoleWriter : IStatisticsHandler
    {
        public void Handle(Guid sessionId, IEnumerable<IStatisticsDto> statisticsDtos)
        {
            if (!statisticsDtos.Any())
            {
                return;
            }

            var aggregates = statisticsDtos.GroupBy(statisticsDto => statisticsDto.Name).Select(dtoAggregate => new
            {
                name = dtoAggregate.Min(dto => dto.Name),
                avg = dtoAggregate.Average(dto => dto.ElapsedMilliseconds),
                min = dtoAggregate.Min(dto => dto.ElapsedMilliseconds),
                max = dtoAggregate.Max(dto => dto.ElapsedMilliseconds),
            });

            Console.Out.WriteLine("Type\t\t\t\tavg\t\tmin\t\tmax\n");
            aggregates.OrderBy(aggregate => aggregate.min).Each(aggregate => Console.Out.WriteLine("{0}:{4}{1}\t\t{2}\t\t{3}", aggregate.name, aggregate.avg, aggregate.min, aggregate.max, aggregate.name.Length < 17 ? "\t\t\t" : aggregate.name.Length < 21 ? "\t\t" : "\t"));
        }
    }
}
