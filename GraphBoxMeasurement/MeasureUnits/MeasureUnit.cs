﻿using GraphBox;
using GraphBoxMeasurement.Dtos;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace GraphBoxMeasurement.MeasureUnits
{
    public abstract class MeasureUnit<RESOLVE, MEASURETYPE>
    {
        protected readonly int _measureIterations;
        protected readonly int _measureRuns;
        protected readonly IContainer _container;
        protected readonly Stopwatch _stopwatch;

        public MeasureUnit(IKeyValueSet MeasureRuns, IKeyValueSet MeasureIterations, IContainer container, Stopwatch stopwatch)
        {
            _measureRuns = Convert.ToInt32(MeasureRuns.Value);
            _measureIterations = Convert.ToInt32(MeasureIterations.Value);
            _container = container;
            _stopwatch = stopwatch;
            Configure(_container);
        }

        public abstract void Configure(IContainer container);

        public IEnumerable<IStatisticsDto> Run()
        {
            Console.Write("Measuring {0}", typeof(MEASURETYPE).Name);
            var results = new List<IStatisticsDto>();

            for (var i = 0; i < _measureRuns; i++)
            {
                yield return MeasureRun();
                Console.Write(".");
            }
            Console.Write("\n");
        }

        public IStatisticsDto MeasureRun()
        {
            _stopwatch.Restart();

            for (var i = 0; i < _measureIterations; i++)
            {
                _container.GetInstance<RESOLVE>();
            }

            _stopwatch.Stop();

            return new StatisticsDto
                { 
                   Name = typeof(MEASURETYPE).Name, 
                   ElapsedMilliseconds = _stopwatch.ElapsedMilliseconds,
                };
        }
    }
}
