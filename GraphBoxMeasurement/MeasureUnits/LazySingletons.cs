﻿using GraphBox;
using System.Diagnostics;

namespace GraphBoxMeasurement.MeasureUnits
{
    public class LazySingletons : MeasureUnit<LazySingletons.ILazySingleton, LazySingletons>, IMeasureUnit
    {
        public LazySingletons(IKeyValueSet MeasureRuns, IKeyValueSet MeasureIterations, IContainer container, Stopwatch stopwatch)
            : base(MeasureRuns, MeasureIterations, container, stopwatch)
        {

        }

        public override void Configure(IContainer container)
        {
            container.Configure(config => config.For<ILazySingleton>().Use<LazySingleton>().AsLazySingleton());
        }

        public interface ILazySingleton
        {
        }

        public class LazySingleton : ILazySingleton
        {
        }
    }
}
