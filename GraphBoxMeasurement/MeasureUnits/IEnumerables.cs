﻿using GraphBox;
using System.Collections.Generic;
using System.Diagnostics;

namespace GraphBoxMeasurement.MeasureUnits
{
    public class IEnumerables : MeasureUnit<IEnumerable<IEnumerables.ITransient>, IEnumerables>, IMeasureUnit
    {
        public IEnumerables(IKeyValueSet MeasureRuns, IKeyValueSet MeasureIterations, IContainer container, Stopwatch stopwatch)
            : base(MeasureRuns, MeasureIterations, container, stopwatch)
        {
        }

        public override void Configure(IContainer container)
        {
            container.Configure(config => config.For<ITransient>().Use<TransientA>());
            container.Configure(config => config.For<ITransient>().Use<TransientB>());
            container.Configure(config => config.For<ITransient>().Use<TransientC>());
        }

        public interface ITransient
        {
        }

        public class TransientA : ITransient
        {
        }

        public class TransientB : ITransient
        {
        }
        
        public class TransientC : ITransient
        {
        }
    }
}
