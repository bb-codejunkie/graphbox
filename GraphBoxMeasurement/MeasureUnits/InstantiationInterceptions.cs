﻿using GraphBox;
using System.Diagnostics;

namespace GraphBoxMeasurement.MeasureUnits
{
    public class InstantiationInterceptions : MeasureUnit<InstantiationInterceptions.ITransient, InstantiationInterceptions>, IMeasureUnit
    {
        public InstantiationInterceptions(IKeyValueSet MeasureRuns, IKeyValueSet MeasureIterations, IContainer container, Stopwatch stopwatch)
            : base(MeasureRuns, MeasureIterations, container, stopwatch)
        {
        }

        public override void Configure(IContainer container)
        {
            container.Configure(config => config.For<ITransient>().Use<Transient>().AndInterceptInstantiationWith<Interceptor>());
        }

        public class Interceptor : IInstantiationInterceptor
        {
            public void Intercept(IInstantiation Instantiation)
            {
            }
        }

        public interface ITransient
        {
        }

        public class Transient : ITransient
        {
        }
    }
}
