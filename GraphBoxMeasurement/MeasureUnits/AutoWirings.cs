﻿using GraphBox;
using System.Diagnostics;

namespace GraphBoxMeasurement.MeasureUnits
{
    public class AutoWirings : MeasureUnit<AutoWirings.IAutoWired, AutoWirings>, IMeasureUnit
    {
        public AutoWirings(IKeyValueSet MeasureRuns, IKeyValueSet MeasureIterations, IContainer container, Stopwatch stopwatch)
            : base(MeasureRuns, MeasureIterations, container, stopwatch)
        {
        }

        public override void Configure(IContainer container)
        {
            container.Configure(config => config.For<ITransient>().Use<Transient>());
            container.Configure(config => config.For<ISingleton>().Use<Singleton>().AsSingleton());
            container.Configure(config => config.For<IAutoWired>().Use<AutoWired>());
        }

        public interface ITransient
        {
        }

        public class Transient : ITransient
        {
        }

        public interface ISingleton
        {
        }

        public class Singleton : ISingleton
        {
        }

        public interface IAutoWired
        {
        }

        public class AutoWired : IAutoWired
        {
            public AutoWired(ISingleton singleton, ITransient transient)
            { }
        }
    }
}
