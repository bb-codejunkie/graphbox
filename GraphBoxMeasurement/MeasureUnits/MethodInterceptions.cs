﻿using GraphBox;
using GraphBox.GraphBoxProxy;
using System.Diagnostics;

namespace GraphBoxMeasurement.MeasureUnits
{
    public class MethodInterceptions : MeasureUnit<MethodInterceptions.ITransient, MethodInterceptions>, IMeasureUnit
    {
        public MethodInterceptions(IKeyValueSet MeasureRuns, IKeyValueSet MeasureIterations, IContainer container, Stopwatch stopwatch)
            : base(MeasureRuns, MeasureIterations, container, stopwatch)
        {
        }

        public override void Configure(IContainer container)
        {
            container.Configure(config => config.For<ITransient>().Use<Transient>().AndInterceptMethodsWith<ProxyInterceptor>());
        }

        public class ProxyInterceptor : IInterceptor
        {
            public void Intercept(IInvocation invocation)
            {
            }
        }

        public interface ITransient
        {
        }

        public class Transient : ITransient
        {
        }
    }
}
