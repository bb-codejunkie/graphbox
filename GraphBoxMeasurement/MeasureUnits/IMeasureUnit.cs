﻿using GraphBoxMeasurement.Dtos;
using System.Collections.Generic;

namespace GraphBoxMeasurement.MeasureUnits
{
    public interface IMeasureUnit
    {
        IEnumerable<IStatisticsDto> Run();
    }
}
