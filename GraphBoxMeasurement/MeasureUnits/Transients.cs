﻿using GraphBox;
using System.Diagnostics;

namespace GraphBoxMeasurement.MeasureUnits
{
    public class Transients : MeasureUnit<Transients.ITransient, Transients>, IMeasureUnit
    {
        public Transients(IKeyValueSet MeasureRuns, IKeyValueSet MeasureIterations, IContainer container, Stopwatch stopwatch)
            : base(MeasureRuns, MeasureIterations, container, stopwatch)
        {
        }

        public override void Configure(IContainer container)
        {
            container.Configure(config => config.For<ITransient>().Use<Transient>());
        }

        public interface ITransient
        {
        }

        public class Transient : ITransient
        {
        }
    }
}
