﻿using System;

namespace GraphBoxMeasurement.Dtos
{
    public interface IStatisticsDto
    {
        long ElapsedMilliseconds { get; }
        string Name { get; }
    }
}
