﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GraphBoxMeasurement.Dtos
{
    public class SessionDto : ISessionDto
    {
        public Guid SessionId { get; private set; }
        public string GitBranch { get; private set; }
        public DateTime Timestamp { get; private set;  }
        public IEnumerable<IStatisticsDto> StatisticsDtos { get; set; }

        public SessionDto()
        {
            SessionId = Guid.NewGuid();
            GitBranch = GetGitBranchName();
            Timestamp = DateTime.Now;
            StatisticsDtos = Enumerable.Empty<IStatisticsDto>();
        }

        private string GetGitBranchName()
        {
            var fullBranch = File.ReadLines(@"..\..\..\.git\HEAD").First();

            return fullBranch.Substring(16);
        }
    }
}
