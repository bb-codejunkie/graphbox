﻿using System;
using System.Collections.Generic;

namespace GraphBoxMeasurement.Dtos
{
    public interface ISessionDto
    {
        string GitBranch { get; }
        Guid SessionId { get; }
        DateTime Timestamp { get; }
        IEnumerable<IStatisticsDto> StatisticsDtos { get; set; }
    }
}
