﻿using GraphBox;
using GraphBox.SubConfigs;
using System.Diagnostics;
using System.Reflection;

namespace GraphBoxMeasurement
{
    public class Program
    {
        static void Main(string[] args)
        {
            var container = new Container();
            container.Configure(config => {
                config.MergeConfig(new SimpleScanner(Assembly.GetExecutingAssembly()));
                config.MergeConfig<ConfigInjection>();
                config.For<Stopwatch>().Use<Stopwatch>();
                config.For<IContainer>().Use<Container>();
            });
            container.GetInstance<IMeasureController>().Start();
        }
    }
}
