﻿using GraphBox;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphBoxMeasurement.Database
{
    public class SqlServerConnection : ISqlServerConnection
    {
        public SqlConnection Connection { get; private set; }

        public SqlServerConnection(IKeyValueSet ConnectionString)
        {
            Connection = new SqlConnection();
            Connection.ConnectionString = ConnectionString.Value;
        }
    }
}
