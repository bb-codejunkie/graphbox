﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphBoxMeasurement.Database
{
    public class CreateTableIfNotExsists : ICreateTableIfNotExsists
    {
        private readonly SqlConnection _connection;
        public CreateTableIfNotExsists(ISqlServerConnection sqlServerConnection)
        {
            _connection = sqlServerConnection.Connection;
        }

        public void Execute(string table, string createSql)
        {
            _connection.Open();

            var sqlCommand = new SqlCommand("if not exists (select * from sys.tables where name='" + table + "') " + createSql, _connection);            
            sqlCommand.ExecuteNonQuery();

            _connection.Close();
        }
    }
}
