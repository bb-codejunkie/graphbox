﻿using System;
namespace GraphBoxMeasurement.Database
{
    public interface ICreateTableIfNotExsists
    {
        void Execute(string table, string createSql);
    }
}
