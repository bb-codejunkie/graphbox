﻿using System;
namespace GraphBoxMeasurement.Database
{
    public interface ISqlServerConnection
    {
        System.Data.SqlClient.SqlConnection Connection { get; }
    }
}
