﻿using GraphBox;
using GraphBoxMeasurement.Dtos;
using GraphBoxMeasurement.MeasureUnits;
using GraphBoxMeasurement.SessionHandlers;
using System.Collections.Generic;
using System.Linq;

namespace GraphBoxMeasurement
{
    public class MeasureController : IMeasureController
    {
        private readonly ISessionDto _sessionDto;
        private readonly IEnumerable<ISessionHandler> _sessionHandlers;
        private readonly IEnumerable<IMeasureUnit> _measureUnits;

        public MeasureController(ISessionDto sessionDto, IEnumerable<ISessionHandler> sessionHandlers, IEnumerable<IMeasureUnit> measureUnits)
        {
            _sessionDto = sessionDto;
            _sessionHandlers = sessionHandlers;
            _measureUnits = measureUnits;
        }

        public void Start()
        {
            _sessionDto.StatisticsDtos = _measureUnits.SelectMany(measureUnit => measureUnit.Run()).ToList();
            _sessionHandlers.Each(sessionHandler => sessionHandler.Handle(_sessionDto));
        }
    }
}
