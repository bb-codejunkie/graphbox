﻿using GraphBoxMeasurement.Dtos;

namespace GraphBoxMeasurement.SessionHandlers
{
    public interface ISessionHandler
    {
        void Handle(ISessionDto sessionDto);
    }
}
