﻿using GraphBoxMeasurement.Database;
using GraphBoxMeasurement.Dtos;
using GraphBoxMeasurement.StatisticsHandlers;
using System.Data.SqlClient;

namespace GraphBoxMeasurement.SessionHandlers
{
    public class SqlServerWriter : ISessionHandler
    {
        private readonly ICreateTableIfNotExsists _createTableIfNotExsists;
        private readonly IStatisticsHandler _statisticsHandler;
        private readonly SqlConnection _connection;
        private readonly string createTableSql =    "CREATE TABLE [dbo].[Sessions](" +
                                                    "[Id] [uniqueidentifier] NOT NULL, " +
                                                    "[GitBranch] [nvarchar](max) NOT NULL, " +
                                                    "[Timestamp] [datetime2](7) NOT NULL, " +
                                                    "[Build] [nchar](8) NOT NULL " +
                                                    ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";

        public SqlServerWriter(ISqlServerConnection sqlServerConnection, ICreateTableIfNotExsists createTableIfNotExsists, IStatisticsHandler SqlServerRawWriter)
        {
            _connection = sqlServerConnection.Connection;
            _createTableIfNotExsists = createTableIfNotExsists;
            _statisticsHandler = SqlServerRawWriter;
        }

        public void Handle(ISessionDto sessionDto)
        {
            _createTableIfNotExsists.Execute("Sessions", createTableSql);
            _connection.Open();

            var sqlCommand = new SqlCommand("INSERT INTO [dbo].[Sessions] (Id, GitBranch, Timestamp, Build) VALUES (@Id, @GitBranch, @Timestamp, @Build)", _connection);
            sqlCommand.Parameters.Add(new SqlParameter("@Id", sessionDto.SessionId));
            sqlCommand.Parameters.Add(new SqlParameter("@GitBranch", sessionDto.GitBranch));
            sqlCommand.Parameters.Add(new SqlParameter("@Timestamp", sessionDto.Timestamp));
#if DEBUG
            sqlCommand.Parameters.Add(new SqlParameter("@Build", "DEBUG"));
#else
            sqlCommand.Parameters.Add(new SqlParameter("@Build", "RELEASE"));
#endif
            sqlCommand.ExecuteNonQuery();

            _connection.Close();

            _statisticsHandler.Handle(sessionDto.SessionId, sessionDto.StatisticsDtos);
        }
    }
}
