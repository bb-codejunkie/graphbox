﻿using FluentAssertions;
using GraphBox;
using GraphBox.Configuration;
using System;
using Xunit;

namespace GraphBoxUnitTests
{
    public class ConfigMergeTests
    {
        public class EmptyConfig : Config
        {
            public EmptyConfig()
            {
            }
        }

        public class TestConfig : Config
        {
            public TestConfig()
            {
                For<INoDependency1>().Use<NoDependencyClass1>();
                For<INoDependency2>().Use<NoDependencyClass2>();
            }
        }

        public class NestedConfig : Config
        {
            public NestedConfig()
            {
                MergeConfig(new SubConfig());
                For<INoDependency1>().Use<NoDependencyClass1>();
            }
        }

        public class SubConfig : Config
        {
            public SubConfig()
            {
                For<INoDependency2>().Use<NoDependencyClass2>();
            }
        }

        public interface INoDependency1
        {
            Guid GetClassGuid();
        }

        public interface INoDependency2
        {
        }

        public class NoDependencyClass1 : INoDependency1
        {
            private readonly Guid _classGuid;

            public NoDependencyClass1()
            {
                _classGuid = Guid.NewGuid();
            }

            public Guid GetClassGuid()
            {
                return _classGuid;
            }
        }

        public class NoDependencyClass2 : INoDependency2
        {
        }

        private readonly Container _container;

        public ConfigMergeTests()
        {
            _container = new Container();
        }

        [Fact]
        public void Given_An_Empty_Config_When_Merging_Configs_Then_The_Test_Runs_Out()
        {
            _container.Configure(x => x.MergeConfig<EmptyConfig>());
        }

        [Fact]
        public void Given_A_Config_When_Merging_Configs_Then_Instances_From_The_Config_Is_Callable()
        {
            _container.Configure(x => x.MergeConfig<TestConfig>());

            var instance1 = _container.GetInstance<INoDependency1>();
            var instance2 = _container.GetInstance<INoDependency2>();

            instance1.Should().BeOfType<NoDependencyClass1>();
            instance2.Should().BeOfType<NoDependencyClass2>();
        }

        [Fact]
        public void Given_A_Nested_Config_When_Merging_Configs_Then_Instances_From_The_Config_Is_Callable()
        {
            _container.Configure(x => x.MergeConfig<NestedConfig>());

            var instance1 = _container.GetInstance<INoDependency1>();
            var instance2 = _container.GetInstance<INoDependency2>();

            instance1.Should().BeOfType<NoDependencyClass1>();
            instance2.Should().BeOfType<NoDependencyClass2>();
        }
    }
}
