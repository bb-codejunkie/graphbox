﻿using System;
using FluentAssertions;
using GraphBox;
using Xunit;
using System.Collections.Generic;

namespace GraphBoxUnitTests
{
    public class ManualWiringTests
    {
        private static Guid _singletonGuid;
        private readonly Container _container;

        public ManualWiringTests()
        {
            _container = new Container();
        }

        [Fact]
        public void Given_A_Type_To_Be_Used_For_Same_Type_When_Getting_Instansce_Of_The_Type_Then_The_Type_Is_Returned()
        {
            _container.Configure(x => x.For<NoDependencyClass1>().Use<NoDependencyClass1>());

            var instance = _container.GetInstance<NoDependencyClass1>();

            instance.Should().BeOfType<NoDependencyClass1>();
        }

        [Fact]
        public void Given_A_Lambda_When_Try_Getting_Then_True_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use(() => new NoDependencyClass1()));

            INoDependency1 instance;
            var isFound = _container.TryGetInstance<INoDependency1>(out instance);

            instance.Should().BeOfType<NoDependencyClass1>();
            isFound.Should().BeTrue();
        }

        [Fact]
        public void Given_A_Valid_Type_When_Try_Getting_Then_True_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            INoDependency1 instance;
            var isFound = _container.TryGetInstance<INoDependency1>(out instance);

            instance.Should().BeOfType<NoDependencyClass1>();
            isFound.Should().BeTrue();
        }

        [Fact]
        public void Given_A_Invalid_Type_When_Try_Getting_Then_False_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            INoDependency2 instance;
            var isFound = _container.TryGetInstance<INoDependency2>(out instance);

            isFound.Should().BeFalse();
        }

        [Fact]
        public void Given_A_valid_Type_When_Try_Getting_By_Name_Then_True_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            INoDependency1 instance;
            var isFound = _container.TryGetInstance<INoDependency1>("NoDependencyClass1", out instance);

            instance.Should().BeOfType<NoDependencyClass1>();
            isFound.Should().BeTrue();
        }

        [Fact]
        public void Given_A_Invalid_Name_When_Try_Getting_By_Name_Then_False_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            INoDependency1 instance;
            var isFound = _container.TryGetInstance<INoDependency1>("NoClass", out instance);

            isFound.Should().BeFalse();
        }

        [Fact]
        public void Given_A_Invalid_Type_When_Try_Getting_By_Name_Then_False_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            INoDependency2 instance;
            var isFound = _container.TryGetInstance<INoDependency2>("NoClass", out instance);

            isFound.Should().BeFalse();
        }

        [Fact]
        public void Given_A_Lambda_When_Try_Getting_From_Child_Container_Then_True_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use(() => new NoDependencyClass1()));

            INoDependency1 instance;
            var isFound = _container.CreateChildContainer().TryGetInstance<INoDependency1>(out instance);

            instance.Should().BeOfType<NoDependencyClass1>();
            isFound.Should().BeTrue();
        }

        [Fact]
        public void Given_A_Valid_Type_When_Try_Getting_From_Child_Container_Then_True_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            INoDependency1 instance;
            var isFound = _container.CreateChildContainer().TryGetInstance<INoDependency1>(out instance);

            instance.Should().BeOfType<NoDependencyClass1>();
            isFound.Should().BeTrue();
        }

        [Fact]
        public void Given_A_Invalid_Type_When_Try_Getting_From_Child_Container_Then_False_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            INoDependency2 instance;
            var isFound = _container.CreateChildContainer().TryGetInstance<INoDependency2>(out instance);

            isFound.Should().BeFalse();
        }

        [Fact]
        public void Given_A_valid_Type_When_Try_Getting_By_Name_From_Child_Container_Then_True_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            INoDependency1 instance;
            var isFound = _container.CreateChildContainer().TryGetInstance<INoDependency1>("NoDependencyClass1", out instance);

            instance.Should().BeOfType<NoDependencyClass1>();
            isFound.Should().BeTrue();
        }

        [Fact]
        public void Given_A_Invalid_Name_When_Try_Getting_By_Name_From_Child_Container_Then_False_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            INoDependency1 instance;
            var isFound = _container.CreateChildContainer().TryGetInstance<INoDependency1>("NoClass", out instance);

            isFound.Should().BeFalse();
        }

        [Fact]
        public void Given_A_Invalid_Type_When_Try_Getting_By_Name_From_Child_Container_Then_False_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            INoDependency2 instance;
            var isFound = _container.CreateChildContainer().TryGetInstance<INoDependency2>("NoClass", out instance);

            isFound.Should().BeFalse();
        }

        [Fact]
        public void Given_A_New_NoDependencyClass1_Expression_When_Getting_Instance_For_INoDependency1_Then_An_Instance_Of_NoDependencyClass1_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use(() => new NoDependencyClass1()));

            var instance = _container.GetInstance<INoDependency1>();

            instance.Should().BeOfType<NoDependencyClass1>();
        }

        [Fact]
        public void Given_A_New_NoDependencyClass1_Expression_When_Getting_Instance_For_INoDependency1_Then_An_Exception_Is_Thrown()
        {
            _container.Configure(x => x.For<INoDependency1>().Use(() => new NoDependencyClass2()));

            Action execute = () => _container.GetInstance<INoDependency1>();

            execute.ShouldThrow<InvalidCastException>();
        }

        [Fact]
        public void Given_A_Single_Configuring_When_Getting_An_Instance_Then_An_Instance_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            var instance = _container.GetInstance<INoDependency1>();

            instance.Should().BeOfType<NoDependencyClass1>();
        }

        [Fact]
        public void Given_A_Single_Configuring_When_Getting_An_Instance_Via_Arg_Then_An_Instance_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            var instance = _container.GetInstance(typeof(INoDependency1));

            instance.Should().BeOfType<NoDependencyClass1>();
        }

        [Fact]
        public void Given_A_Single_Configuring_When_Getting_An_Instance_By_Name_Then_An_Instance_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            var instance = _container.GetInstance<INoDependency1>("NoDependencyClass1");

            instance.Should().BeOfType<NoDependencyClass1>();
        }

        [Fact]
        public void Given_A_Single_Configuring_When_Getting_An_Instance_By_Type_By_Name_Then_An_Instance_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            var instance = _container.GetInstance(typeof(INoDependency1), "NoDependencyClass1");

            instance.Should().BeOfType<NoDependencyClass1>();
        }

        [Fact]
        public void Given_2_Implementations_And_1_Interface_When_Getting_Instances_Then_Right_Instances_Are_Returned()
        {
            _container.Configure(x =>
            {
                x.For<INoDependency1>().Use<NoDependencyClass1>();
                x.For<INoDependency1>().Use<NoDependencyClass1_2>();
            });

            var instance1 = _container.GetInstance<INoDependency1>("NoDependencyClass1");
            var instance2 = _container.GetInstance<INoDependency1>("NoDependencyClass1_2");

            instance1.Should().BeOfType<NoDependencyClass1>();
            instance2.Should().BeOfType<NoDependencyClass1_2>();
        }

        [Fact]
        public void Given_More_Configures_When_Getting_Instances_Then_Right_Instance_Is_Returned()
        {
            _container.Configure(x =>
            {
                x.For<INoDependency1>().Use<NoDependencyClass1>();
                x.For<INoDependency2>().Use<NoDependencyClass2>();
            });

            var instance1 = _container.GetInstance<INoDependency1>();
            var instance2 = _container.GetInstance<INoDependency2>();

            instance1.Should().BeOfType<NoDependencyClass1>();
            instance2.Should().BeOfType<NoDependencyClass2>();
        }

        [Fact]
        public void Given_A_Configured_Singleton_When_Getting_More_Instances_For_A_Type_Then_The_Refs_Of_The_Instances_Is_Equal()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AsSingleton());

            var instance1 = _container.GetInstance<INoDependency1>();
            var instance2 = _container.GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().Be(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_Configured_Lazy_Singleton_When_Getting_More_Instances_For_A_Type_Then_The_Refs_Of_The_Instances_Is_Equal()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AsLazySingleton());

            var instance1 = _container.GetInstance<INoDependency1>();
            var instance2 = _container.GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().Be(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_Single_Configuration_When_Getting_More_Instances_For_A_Type_Then_The_Refs_Of_The_Instances_Is_Not_The_Same()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());

            var instance1 = _container.GetInstance<INoDependency1>();
            var instance2 = _container.GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().NotBe(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_Singleton_When_Configuring_Then_The_Singleton_Is_Instanciated()
        {
            _singletonGuid = Guid.Empty;
            
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AsSingleton());

            _singletonGuid.Should().NotBe(Guid.Empty);
        }

        [Fact]
        public void Given_A_Lazy_Singleton_When_Configuring_Then_The_Lazy_Singleton_Is_Not_Instanciated()
        {
            _singletonGuid = Guid.Empty;

            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AsLazySingleton());

            _singletonGuid.Should().Be(Guid.Empty);
        }

        public interface INoDependency1
        {
            Guid GetClassGuid();
        }

        public interface INoDependency2
        {
        }

        public class NoDependencyClass1 : INoDependency1
        {
            private readonly Guid _classGuid;

            public NoDependencyClass1()
            {
                _singletonGuid = _classGuid = Guid.NewGuid();
            }

            public Guid GetClassGuid()
            {
                return _classGuid;
            }
        }

        public class NoDependencyClass1_2 : INoDependency1
        {
            private readonly Guid _classGuid;

            public NoDependencyClass1_2()
            {
                _classGuid = Guid.NewGuid();
            }

            public Guid GetClassGuid()
            {
                return _classGuid;
            }
        }

        public class NoDependencyClass2 : INoDependency2
        {
        }
    }
}