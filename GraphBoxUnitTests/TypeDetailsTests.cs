﻿using FluentAssertions;
using GraphBox;
using GraphBox.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace GraphBoxUnitTests
{
    public class TypeDetailsTests
    {
        [Fact]
        public void Given_Typedetails_When_Cloning_TypeDetails_Then_The_Clone_Is_Identical()
        {
            var typeDetails = new TypeDetails();
            ArrangeTypeDetails(typeDetails);

            var clonedTypeDetails = (TypeDetails)typeDetails.Clone();

            TestAllFields(typeDetails, clonedTypeDetails).Should().BeTrue();
        }

        private bool TestAllFields(TypeDetails originalTypedetails, TypeDetails clonedTypedetails)
        {
            var fields = originalTypedetails.GetType().GetFields();

            var allEqual = true;
            fields.Each(field => {
                var fieldName = field.Name;

                bool isIdentical;
                if (field.FieldType == typeof(bool))
                {
                    isIdentical = (bool)field.GetValue(originalTypedetails) == (bool)field.GetValue(clonedTypedetails);
                }
                else if (field.FieldType == typeof(Guid))
                {
                    isIdentical = (Guid)field.GetValue(originalTypedetails) == (Guid)field.GetValue(clonedTypedetails);                   
                }
                else
                {
                    isIdentical = field.GetValue(originalTypedetails) == field.GetValue(clonedTypedetails);
                }

                if (!isIdentical)
                {
                    allEqual = false;
                }
            });

            return allEqual;
        }

        private void ArrangeTypeDetails(TypeDetails typedetails)
        {
            var fields = typedetails.GetType().GetFields();

            fields.Each(field => { 

                if (field.FieldType == typeof(bool))
                {
                    field.SetValue(typedetails, !(bool)field.GetValue(typedetails));
                    return;
                }

                if (field.FieldType == typeof(DelegateFactory.CreateInstance))
                {
                    DelegateFactory.CreateInstance createInstance = CreateInstanceDummy;
                    field.SetValue(typedetails, createInstance);
                    return;
                }

                if (field.FieldType == typeof(TypeDetails[]))
                {
                    var typeDetails = new TypeDetails[] { };
                    field.SetValue(typedetails, typeDetails);
                    return;
                }

                if (field.FieldType == typeof(Type))
                {
                    field.SetValue(typedetails, typeof(TypeDetails));
                    return;
                }

                if (field.FieldType == typeof(object))
                {
                    field.SetValue(typedetails, new TypeDetails());
                    return;
                }

                if (field.FieldType == typeof(Func<object>))
                {
                    Func<object> function = () => { return new TypeDetails(); };
                    field.SetValue(typedetails, function);
                    return;
                }

                if (field.FieldType == typeof(IEnumerable<TypeDetails>))
                {
                    field.SetValue(typedetails, Enumerable.Empty<TypeDetails>());
                    return;
                }

                if (field.FieldType == typeof(Guid))
                {
                    field.SetValue(typedetails, new Guid("9711E4E2-EDA4-4FC9-BFEE-50929ED5C25A"));
                    return;
                }

                if (field.FieldType == typeof(IInstantiationInterceptor))
                {
                    field.SetValue(typedetails, new DummyInterceptor());
                    return;
                }

                throw new ArgumentException();
            });
        }

        public void CreateInstanceDummy(TypeDetails typeDetails, out object output)
        {
            output = null;
        }

        public class DummyInterceptor : IInstantiationInterceptor
        {
            public void Intercept(IInstantiation Instantiation)
            {
                throw new NotImplementedException();
            }
        }
    }
}
