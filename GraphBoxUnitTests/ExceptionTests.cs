﻿using FluentAssertions;
using GraphBox;
using System;
using Xunit;

namespace GraphBoxUnitTests
{
    public class ExceptionTests
    {
        private readonly Container _container;

        public ExceptionTests()
        {
            _container = new Container();
        }

        [Fact]
        public void Given_A_Empty_Container_When_Getting_Instance_The_An_Exception_Is_Thrown()
        {
            Action exexute = () => _container.GetInstance<IRandomClass>();

            exexute.ShouldThrow<Exception>().WithMessage("Unknown type: IRandomClass");
        }

        [Fact]
        public void Given_A_Configured_Container_When_Getting_Instance_By_Wrong_Name_The_An_Exception_Is_Thrown()
        {
            _container.Configure(x => x.For<IRandomClass>().Use<RandomClass>());

            Action exexute = () => _container.GetInstance<IRandomClass>("UnknownClass");

            exexute.ShouldThrow<Exception>().WithMessage("Unknown type: UnknownClass for IRandomClass");
        }

        [Fact]
        public void Given_A_Empty_Container_When_Getting_Instance_By_Name_Then_An_Exception_Is_Thrown()
        {
            Action exexute = () => _container.GetInstance<IRandomClass>("RandomClass");

            exexute.ShouldThrow<Exception>().WithMessage("Unknown type: IRandomClass");
        }

        [Fact]
        public void Given_A_Class_With_Unregistred_Dependencies_When_Configuring_Autowiring_Then_An_Exception_Is_Thrown()
        {
            Action exexute = () => _container.Configure(x => x.For<IDependencyClass>().Use<DependencyClass>());

            exexute.ShouldThrow<Exception>().WithMessage("Unknown type: IRandomClass");
        }

        [Fact]
        public void Given_A_Class_With_Unregistred_Dependencies_When_Configuring_Named_Autowiring_Then_An_Exception_Is_Thrown()
        {
            Action exexute = () => _container.Configure(x => {
                x.For<IRandomClass>().Use<RandomClass>();
                x.For<IRandomClass>().Use<RandomClass2>();
                x.For<IDependencyClass>().Use<DependencyClass2>();
            });

            exexute.ShouldThrow<Exception>().WithMessage("Unknown type: RandomClass3 for IRandomClass");
        }

        public interface IDependencyClass
        {

        }

        public interface IRandomClass
        {

        }

        public class RandomClass : IRandomClass
        {

        }

        public class RandomClass2 : IRandomClass
        {

        }

        public class DependencyClass : IDependencyClass
        {
            public DependencyClass(IRandomClass randomClass)
            {

            }
        }

        public class DependencyClass2 : IDependencyClass
        {
            public DependencyClass2(IRandomClass RandomClass, IRandomClass RandomClass3)
            {

            }
        }
    }
}
