﻿using FluentAssertions;
using GraphBox;
using GraphBox.Configuration;
using System;
using Xunit;

namespace GraphBoxUnitTests
{
    public class ChildContainerTests
    {
        static public bool _diposeWasCalled;

        public interface INoDependency1 : IDisposable
        {
            Guid GetClassGuid();
        }

        public class NoDependencyClass1 : INoDependency1
        {
            private readonly Guid _classGuid;

            public NoDependencyClass1()
            {
                _classGuid = Guid.NewGuid();
            }

            public Guid GetClassGuid()
            {
                return _classGuid;
            }

            public void Dispose()
            {
                _diposeWasCalled = true;
            }
        }

        private readonly Container _container;

        public ChildContainerTests()
        {
            _container = new Container();
            _diposeWasCalled = false;
        }

        [Fact]
        public void Given_A_Type_A_Singleton_When_Getting_Instance_From_Child_Container_Then_Singleton_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>()
                                            .AsSingleton());
            var instance1 = _container.GetInstance<INoDependency1>();

            var instance2 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().Be(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_Type_A_Transient_When_Getting_Instance_From_Child_Container_Then_Singleton_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>());
            var instance1 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            var instance2 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().NotBe(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_Type_A_Lifetime_Scoped_Singleton_When_Getting_Instance_From_Child_Container_Then_A_New_Singleton_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>()
                                            .AsSingleton().WithPerContainerLifeTime());
            var instance1 = _container.GetInstance<INoDependency1>();

            var instance2 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().NotBe(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_Type_A_Singleton_And_A_Child_Container_When_Getting_Instance_From_Child_Container_Then_A_New_Singleton_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>()
                                            .AsSingleton());
            var instance1 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            var instance2 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().Be(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_Type_A_Lifetime_Scoped_Singleton_And_A_Child_Container_When_Getting_Instance_From_Child_Container_Then_A_New_Singleton_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>()
                                            .AsSingleton().WithPerContainerLifeTime());
            var instance1 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            var instance2 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().NotBe(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_IDisposeable_Per_Instance_Singleton_When_Disposing_A_Child_Container_Then_The_Singletons_Dispose_Method_Is_Called()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>()
                                            .AsSingleton().WithPerContainerLifeTime());
            var spawnedContainer = _container.CreateChildContainer();
            _diposeWasCalled = false;

            spawnedContainer.Dispose();

            _diposeWasCalled.Should().BeTrue();
        }

        [Fact]
        public void Given_A_IDisposeable_Singleton_When_Disposing_A_Child_Container_Then_The_Singletons_Dispose_Method_Is_Not_Called()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>()
                                            .AsSingleton());
            var spawnedContainer = _container.CreateChildContainer();
            _diposeWasCalled = false;

            spawnedContainer.Dispose();

            _diposeWasCalled.Should().BeFalse();
        }

        [Fact]
        public void Given_A_IDisposeable_Transient_When_Disposing_A_Child_Container_Then_The_Singletons_Dispose_Method_Is_Called()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>());
            var spawnedContainer = _container.CreateChildContainer();
            spawnedContainer.GetInstance<INoDependency1>();
            _diposeWasCalled = false;

            spawnedContainer.Dispose();

            _diposeWasCalled.Should().BeTrue();
        }















        [Fact]
        public void Given_A_Type_A_LazySingleton_When_Getting_Instance_From_Child_Container_Then_Singleton_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>()
                                            .AsLazySingleton());
            var instance1 = _container.GetInstance<INoDependency1>();

            var instance2 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().Be(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_Type_A_Lifetime_Scoped_LazySingleton_When_Getting_Instance_From_Child_Container_Then_A_New_Singleton_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>()
                                            .AsLazySingleton().WithPerContainerLifeTime());
            var instance1 = _container.GetInstance<INoDependency1>();

            var instance2 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().NotBe(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_Type_A_LazySingleton_And_A_Child_Container_When_Getting_Instance_From_Child_Container_Then_A_New_Singleton_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>()
                                            .AsLazySingleton());
            var instance1 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            var instance2 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().Be(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_Type_A_Lifetime_Scoped_LazySingleton_And_A_Child_Container_When_Getting_Instance_From_Child_Container_Then_A_New_Singleton_Is_Returned()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>()
                                            .AsLazySingleton().WithPerContainerLifeTime());
            var instance1 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            var instance2 = _container.CreateChildContainer().GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().NotBe(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_IDisposeable_Per_Instance_LazySingleton_When_Disposing_A_Child_Container_Then_The_Singletons_Dispose_Method_Is_Called()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>()
                                            .AsLazySingleton().WithPerContainerLifeTime());
            var spawnedContainer = _container.CreateChildContainer();
            _diposeWasCalled = false;

            spawnedContainer.Dispose();

            _diposeWasCalled.Should().BeFalse();
        }

        [Fact]
        public void Given_A_IDisposeable_LazySingleton_When_Disposing_A_Child_Container_Then_The_Singletons_Dispose_Method_Is_Not_Called()
        {
            _container.Configure(x => x.For<INoDependency1>()
                                        .Use<NoDependencyClass1>()
                                            .AsLazySingleton());
            var spawnedContainer = _container.CreateChildContainer();
            _diposeWasCalled = false;

            spawnedContainer.Dispose();

            _diposeWasCalled.Should().BeFalse();
        }
    }
}
