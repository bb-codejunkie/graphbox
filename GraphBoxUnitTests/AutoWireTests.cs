﻿using FluentAssertions;
using GraphBox;
using Xunit;

namespace GraphBoxUnitTests
{
    public class AutoWireTests
    {
        private readonly Container _container;

        public AutoWireTests()
        {
            _container = new Container();
        }

        [Fact]
        public void Given_A_Type_To_Use_For_The_Same_Type_Which_Have_Depencies_When_Getting_An_Instance_Of_The_Type_Then_The_Type_Is_Returned()
        {
            _container.Configure(x =>
            {
                x.For<INoDependency1>().Use<NoDependencyClass1>();
                x.For<INoDependency2>().Use<NoDependencyClass2>();
            });
            _container.Configure(x => x.For<DependencyClass1>().Use<DependencyClass1>());

            var instance = _container.GetInstance<DependencyClass1>();

            instance.Should().BeOfType<DependencyClass1>();
        }

        [Fact]
        public void Given_A_Lambda_Functions_As_Dependency_When_Getting_A_Instance_Then_The_Instance_Is_Returned()
        {
            _container.Configure(x =>
            {
                x.For<INoDependency1>().Use<NoDependencyClass1>();
                x.For<INoDependency2>().Use(() => new NoDependencyClass2());
            });
            _container.Configure(x => x.For<DependencyClass1>().Use<DependencyClass1>());

            var instance = _container.GetInstance<DependencyClass1>();

            instance.Should().BeOfType<DependencyClass1>();
        }

        [Fact]
        public void Given_A_Config_In_2_Steps_When_Getting_A_Dependency1_Instance_Then_Correct_Instance_Is_Returned()
        {
            _container.Configure(x =>
            {
                x.For<INoDependency1>().Use<NoDependencyClass1>();
                x.For<INoDependency2>().Use<NoDependencyClass2>();
            });
            _container.Configure(x => x.For<IDependency1>().Use<DependencyClass1>());

            var instance = _container.GetInstance<IDependency1>();

            instance.Should().BeOfType<DependencyClass1>();
        }

        [Fact]
        public void Given_2_Levels_Of_Dependencies_When_Getting_An_Instance_Then_The_Instance_Is_Returned()
        {
            _container.Configure(x =>
            {
                x.For<INoDependency1>().Use<NoDependencyClass1>();
                x.For<INoDependency2>().Use<NoDependencyClass2>();

                x.For<IDependency1>().Use<DependencyClass1>();
            });

            var instance = _container.GetInstance<IDependency1>();

            instance.Should().BeOfType<DependencyClass1>();
        }

        [Fact]
        public void Given_2_Levels_Of_Dependencies_Where_One_Of_Them_Is_Singleton_When_Getting_An_Instance_Then_The_Instance_Is_Returned()
        {
            _container.Configure(x =>
            {
                x.For<INoDependency1>().Use<NoDependencyClass1>().AsSingleton();
                x.For<INoDependency2>().Use<NoDependencyClass2>();

                x.For<IDependency1>().Use<DependencyClass1>();
            });

            var instance = _container.GetInstance<IDependency1>();

            instance.Should().BeOfType<DependencyClass1>();
        }

        [Fact]
        public void Given_3_Levels_Of_Dependencies_When_Getting_An_Instance_Then_The_Instance_Is_Returned()
        {
            _container.Configure(x =>
            {
                x.For<INoDependency1>().Use<NoDependencyClass1>();
                x.For<INoDependency2>().Use<NoDependencyClass2>();

                x.For<IDependency1>().Use<DependencyClass1>();
                x.For<IDependency2>().Use<DependencyClass2>();
            });

            var instance = _container.GetInstance<IDependency2>();

            instance.Should().BeOfType<DependencyClass2>();
        }

        public class DependencyClass1 : IDependency1
        {
            private INoDependency1 _noDependency1;
            private INoDependency2 _noDependency2;

            public DependencyClass1(INoDependency1 noDependency1, INoDependency2 noDependency2)
            {
                _noDependency1 = noDependency1;
                _noDependency2 = noDependency2;
            }
        }

        public class DependencyClass2 : IDependency2
        {
            private IDependency1 _dependency1;
            private INoDependency1 _noDependency1;
            private INoDependency2 _noDependency2;

            public DependencyClass2(INoDependency1 noDependency1, INoDependency2 noDependency2, IDependency1 dependency1)
            {
                _noDependency1 = noDependency1;
                _noDependency2 = noDependency2;
                _dependency1 = dependency1;
            }
        }

        public interface IDependency1
        {
        }

        public interface IDependency2
        {
        }

        public interface INoDependency1
        {
        }

        public interface INoDependency2
        {
        }

        public class NoDependencyClass1 : INoDependency1
        {
        }

        public class NoDependencyClass2 : INoDependency2
        {
        }
    }
}