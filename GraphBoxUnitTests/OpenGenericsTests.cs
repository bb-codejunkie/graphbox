﻿using GraphBox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;

namespace GraphBoxUnitTests
{
    public class OpenGenericsTests
    {
        private readonly Container _container;

        public OpenGenericsTests()
        {
            _container = new Container();
        }

        [Fact]
        public void Given_A_Config_With_Open_Generics_When_Setting_Ids_On_Generic_Instance_Then_The_Values_Are_Set()
        {
            _container.Configure(x => x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass<,>)));
            var instance = _container.GetInstance<IGenericClass<Guid, int>>();
            var Id = Guid.NewGuid();
            var Id2 = 42;

            instance.Id = Id;
            instance.Id2 = Id2;

            instance.Id.Should().Be(Id);
            instance.Id2.Should().Be(Id2);
        }

        [Fact]
        public void Given_A_Config_With_Open_Generics_When_Setting_Ids_On_Generic_Instance_From_A_Child_Container_Then_The_Values_Are_Set()
        {
            _container.Configure(x => x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass<,>)));
            var instance = _container.CreateChildContainer().GetInstance<IGenericClass<Guid, int>>();
            var Id = Guid.NewGuid();
            var Id2 = 42;

            instance.Id = Id;
            instance.Id2 = Id2;

            instance.Id.Should().Be(Id);
            instance.Id2.Should().Be(Id2);
        }

        [Fact]
        public void Given_A_Autowired_Setup_With_1to1_Wired_Generics_When_Getting_Ids_Then_The_Right_Ids_Are_returned()
        {
            _container.Configure(x =>
            {
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass<,>));
                x.For(typeof(TopClass<,>)).Use(typeof(TopClass<,>));
            });
            var instance = _container.GetInstance<TopClass<Guid, int>>();
            var Id = Guid.NewGuid();
            var Id2 = 42;

            instance.Id = Id;
            instance.Id2 = Id2;

            instance.Id.Should().Be(Id);
            instance.Id2.Should().Be(Id2);
        }

        [Fact]
        public void Given_A_Autowired_Setup_With_Splitted_Generics_When_Getting_Ids_Then_The_Right_Ids_Are_returned()
        {
            _container.Configure(x =>
            {
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass<,>));
                x.For(typeof(IGenericClass<>)).Use(typeof(GenericClass<>));
                x.For(typeof(TopClass<,,>)).Use(typeof(TopClass<,,>));
            });
            var instance = _container.GetInstance<TopClass<Guid, int, string>>();
            var Id = Guid.NewGuid();
            var Id2 = 42;
            var Id3 = "Abc";

            instance.Id = Id;
            instance.Id2 = Id2;
            instance.Id3 = Id3;

            instance.Id.Should().Be(Id);
            instance.Id2.Should().Be(Id2);
            instance.Id3.Should().Be(Id3);
        }

        [Fact]
        public void Given_A_Autowired_Setup_With_A_Closed_Generic_When_Getting_Ids_Then_The_Right_Ids_Are_returned()
        {
            _container.Configure(x =>
            {
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass<,>));
                x.For(typeof(TopClass)).Use(typeof(TopClass));
            });
            var instance = _container.GetInstance<TopClass>();
            var Id = Guid.NewGuid();
            var Id2 = 42;

            instance.Id = Id;
            instance.Id2 = Id2;

            instance.Id.Should().Be(Id);
            instance.Id2.Should().Be(Id2);
        }

        [Fact]
        public void Given_A_Autowired_Setup_With_A_Closed_And_An_Open_Generic_When_Getting_Ids_Then_The_Right_Ids_Are_returned()
        {
            _container.Configure(x =>
            {
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass<,>));
                x.For(typeof(IGenericClass<>)).Use(typeof(GenericClass<>));
                x.For(typeof(TopClass<>)).Use(typeof(TopClass<>));
            });
            var instance = _container.GetInstance<TopClass<string>>();
            var Id = Guid.NewGuid();
            var Id2 = 42;
            var Id3 = "Abc";

            instance.Id = Id;
            instance.Id2 = Id2;
            instance.Id3 = Id3;

            instance.Id.Should().Be(Id);
            instance.Id2.Should().Be(Id2);
            instance.Id3.Should().Be(Id3);
        }

        [Fact]
        public void Given_Three_Implementations_To_One_Interface_When_Counting_Number_Of_Instances_Then_The_Count_Is_Three()
        {
            _container.Configure(x =>{
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass<,>));
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass1<,>));
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass2<,>));
            });

            var instances = _container.GetInstance<IEnumerable<IGenericClass<Guid, int>>>();

            instances.Count().Should().Be(3);
        }

        [Fact]
        public void Given_Three_Generic_Implementations_To_One_Interface_And_AutoWiring_When_Counting_Number_Of_Instances_Then_The_Count_Is_Three()
        {
            _container.Configure(x =>
            {
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass<,>));
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass1<,>));
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass2<,>));
                x.For(typeof(IEnumerableTopClass)).Use(typeof(IEnumerableTopClass));
            });

            var instance = _container.GetInstance<IEnumerableTopClass>();

            instance.GenericClasses.Count().Should().Be(3);
        }

        [Fact]
        public void Given_Three_Generic_Implementations_To_One_Interface_And_AutoWiring_And_Open_Generic_in_contructor_When_Counting_Number_Of_Instances_Then_The_Count_Is_Three()
        {
            _container.Configure(x =>
            {
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass<,>));
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass1<,>));
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass2<,>));
                x.For(typeof(IEnumerableTopClass<,>)).Use(typeof(IEnumerableTopClass<,>));
            });

            var instance = _container.GetInstance<IEnumerableTopClass<int, string>>();

            instance.GenericClasses.Count().Should().Be(3);
        }

        [Fact]
        public void Given_2_IEnumerables_With_Open_Generics_And_AutoWiring_And_Open_Generic_in_contructor_When_Counting_Number_Of_Instances_Then_The_Count_Is_Three()
        {
            _container.Configure(x =>
            {
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass<,>));
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass1<,>));
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass2<,>));
                x.For(typeof(IGenericClass<>)).Use(typeof(GenericClass<>));
                x.For(typeof(IEnumerableTopClass<,,>)).Use(typeof(IEnumerableTopClass<,,>));
            });

            var instance = _container.GetInstance<IEnumerableTopClass<int, string, Guid>>();

            instance.GenericClasses.Count().Should().Be(3);
        }

        [Fact]
        public void Given_One_Interface_To_Many_Implementations_Setup_When_Getting_Instance_By_Name_Then_The_Right_Instance_Is_Returned()
        {
            _container.Configure(x =>
            {
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass<,>));
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass1<,>));
                x.For(typeof(IGenericClass<,>)).Use(typeof(GenericClass2<,>));
            });

            var instance = _container.GetInstance<IGenericClass<int, string>>("GenericClass1`2");

            instance.GetType().Name.Should().Be("GenericClass1`2");
        }


        [Fact]
        public void Given_A_Closed_Generics_Setup_When_Getting_Instance_Then_The_Right_Instance_Is_Returned()
        {
            _container.Configure(x =>
            {
                x.For(typeof(IGenericClass<int, string>)).Use(typeof(GenericClass<int, string>));
            });

            var instance = _container.GetInstance<IGenericClass<int, string>>();

            instance.GetType().Name.Should().Be("GenericClass`2");
        }
    }

    public interface IGenericClass<T, U>
    {
        T Id { get; set; }
        U Id2 { get; set; }
    }

    public class GenericClass<T, U> : IGenericClass<T, U>
    {
        public T Id { get; set; }
        public U Id2 { get; set; }
    }

    public class GenericClass1<T, U> : IGenericClass<T, U>
    {
        public T Id { get; set; }
        public U Id2 { get; set; }
    }

    public class GenericClass2<T, U> : IGenericClass<T, U>
    {
        public T Id { get; set; }
        public U Id2 { get; set; }
    }

    public interface IGenericClass<T>
    {
        T Id { get; set; }
    }

    public class GenericClass<T> : IGenericClass<T>
    {
        public T Id { get; set; }
    }

    public class IEnumerableTopClass<T, U, Z>
    {
        public IEnumerable<IGenericClass<T, U>> GenericClasses { get; set; }
        public IEnumerable<IGenericClass<Z>> GenericClasses2 { get; set; }

        public IEnumerableTopClass(IEnumerable<IGenericClass<T, U>> genericClasses, IEnumerable<IGenericClass<Z>> genericClasses2)
        {
            GenericClasses = genericClasses;
            GenericClasses2 = genericClasses2;
        }
    }

    public class IEnumerableTopClass<T, U>
    {
        public IEnumerable<IGenericClass<T, U>> GenericClasses { get; set; }

        public IEnumerableTopClass(IEnumerable<IGenericClass<T, U>> genericClasses)
        {
            GenericClasses = genericClasses;
        }
    }

    public class IEnumerableTopClass
    {
        public IEnumerable<IGenericClass<Guid, int>> GenericClasses { get; set; }

        public IEnumerableTopClass(IEnumerable<IGenericClass<Guid, int>> genericClasses)
        {
            GenericClasses = genericClasses;
        }
    }

    public class TopClass
    {
        public Guid Id { get; set; }
        public int Id2 { get; set; }

        public TopClass(IGenericClass<Guid, int> genericClass)
        {
            Id = genericClass.Id;
            Id2 = genericClass.Id2;
        }
    }

    public class TopClass<T>
    {
        public Guid Id { get; set; }
        public int Id2 { get; set; }
        public T Id3 { get; set; }

        public TopClass(IGenericClass<Guid, int> closedGenericClass, IGenericClass<T> openGeneric)
        {
            Id = closedGenericClass.Id;
            Id2 = closedGenericClass.Id2;
            Id3 = openGeneric.Id;
        }
    }

    public class TopClass<T, U>
    {
        public T Id { get; set; }
        public U Id2 { get; set; }

        public TopClass(IGenericClass<T, U> genericClass)
        {
            Id = genericClass.Id;
            Id2 = genericClass.Id2;
        }
    }

    public class TopClass<T, U, V>
    {
        public T Id { get; set; }
        public U Id2 { get; set; }
        public V Id3 { get; set; }

        public TopClass(IGenericClass<T, U> genericClass, IGenericClass<V> genericClassV)
        {
            Id = genericClass.Id;
            Id2 = genericClass.Id2;
            Id3 = genericClassV.Id;
        }
    }
}
