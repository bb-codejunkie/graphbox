﻿using GraphBox;
using System;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;

namespace GraphBoxUnitTests
{
    public class InstantiationInterceptionTest
    {
        private readonly Container _container;
        private static IEnumerable<Type> _argTypes;
        private static string _newNoDependency1InstanceGuid;

        public InstantiationInterceptionTest()
        {
            _container = new Container();
            _container.Configure(x =>
            {
                x.For<INoDependency1>().Use<NoDependencyClass1>();
                x.For<INoDependency2>().Use<NoDependencyClass2>();

                x.For<IDependency1>().Use<DependencyClass1>();
            });
        }

        [Fact]
        public void Given_A_Type_With_Instantiation_Interception_When_Getting_An_Instance_Then_The_Invocation_Hold_The_Right_Arguments()
        {
            IEnumerable<Type> expectedTypes = new List<Type> {typeof(NoDependencyClass1), typeof(NoDependencyClass2), typeof(DependencyClass1)};
            _container.Configure(x =>
            {
                x.For<IDependency2>().Use<DependencyClass2>().AndInterceptInstantiationWith<Interceptor1>();
            });

            _container.GetInstance<IDependency2>();

            _argTypes.ShouldAllBeEquivalentTo(expectedTypes);
        }

        [Fact]
        public void Given_A_Type_With_Instantiation_Interception_When_Getting_A_Lazy_Singleton_Instance_Then_The_Invocation_Hold_The_Right_Arguments()
        {
            IEnumerable<Type> expectedTypes = new List<Type> { typeof(NoDependencyClass1), typeof(NoDependencyClass2), typeof(DependencyClass1) };
            _container.Configure(x =>
            {
                x.For<IDependency2>().Use<DependencyClass2>().AsLazySingleton().AndInterceptInstantiationWith<Interceptor1>();
            });

            _container.GetInstance<IDependency2>();

            _argTypes.ShouldAllBeEquivalentTo(expectedTypes);
        }

        [Fact]
        public void Given_A_Type_With_Instantiation_Interception_When_Getting_A_Singleton_Instance_Then_The_Invocation_Hold_The_Right_Arguments()
        {
            IEnumerable<Type> expectedTypes = new List<Type> { typeof(NoDependencyClass1), typeof(NoDependencyClass2), typeof(DependencyClass1) };
            _container.Configure(x =>
            {
                x.For<IDependency2>().Use<DependencyClass2>().AsSingleton().AndInterceptInstantiationWith<Interceptor1>();
            });

            _container.GetInstance<IDependency2>();

            _argTypes.ShouldAllBeEquivalentTo(expectedTypes);
        }

        [Fact]
        public void Given_A_Type_With_Instantiation_Interception_And_Changing_Injection_When_Getting_An_Instance_Then_The_Guid_Match()
        {
            IEnumerable<Type> expectedTypes = new List<Type> { typeof(NoDependencyClass1), typeof(NoDependencyClass2), typeof(DependencyClass1) };
            _container.Configure(x =>
            {
                x.For<IDependency2>().Use<DependencyClass2>().AndInterceptInstantiationWith<Interceptor2>();
            });

            var instance = _container.GetInstance<IDependency2>();

            instance.GetGuid().Should().Be(_newNoDependency1InstanceGuid);
        }

        public class Interceptor1 : IInstantiationInterceptor
        {
            public void Intercept(IInstantiation Instantiation)
            {
                _argTypes = Instantiation.Arguments;

                Instantiation.Proceed();
            }
        }

        public class Interceptor2 : IInstantiationInterceptor
        {
            public void Intercept(IInstantiation Instantiation)
            {
                var newNoDependency1Instance = new NoDependencyClass1();
                _newNoDependency1InstanceGuid = newNoDependency1Instance.GetGuid();
                Instantiation.InstancesToBeInjected[0] = newNoDependency1Instance;
 
                Instantiation.Proceed();
            }
        }

        public class DependencyClass1 : IDependency1
        {
            private INoDependency1 _noDependency1;
            private INoDependency2 _noDependency2;

            public DependencyClass1(INoDependency1 noDependency1, INoDependency2 noDependency2)
            {
                _noDependency1 = noDependency1;
                _noDependency2 = noDependency2;
            }
        }

        public class DependencyClass2 : IDependency2
        {
            private IDependency1 _dependency1;
            private INoDependency1 _noDependency1;
            private INoDependency2 _noDependency2;

            public DependencyClass2(INoDependency1 noDependency1, INoDependency2 noDependency2, IDependency1 dependency1)
            {
                _noDependency1 = noDependency1;
                _noDependency2 = noDependency2;
                _dependency1 = dependency1;
            }

            public string GetGuid()
            {
                return _noDependency1.GetGuid();
            }
        }

        public interface IDependency1
        {
        }

        public interface IDependency2
        {
            string GetGuid();
        }

        public interface INoDependency1
        {
            string GetGuid();
        }

        public interface INoDependency2
        {
        }

        public class NoDependencyClass1 : INoDependency1
        {
            private string _guid = Guid.NewGuid().ToString();

            public string GetGuid()
            {
                return _guid;
            }
        }

        public class NoDependencyClass2 : INoDependency2
        {
        }
    }
}
