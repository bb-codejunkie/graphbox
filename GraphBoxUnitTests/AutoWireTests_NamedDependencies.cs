﻿using FluentAssertions;
using GraphBox;
using System;
using Xunit;

namespace GraphBoxUnitTests
{
    public class AutoWireTests_NamedDependencies
    {
        public AutoWireTests_NamedDependencies()
        { 
        }

        [Fact]
        public void Given_An_Container_With_One_Interface_To_Many_Implementations_When_Getting_An_Instance_Then_The_Implementations_Are_Resolved()
        {
            var container = new Container();
            container.Configure(x =>
            {
                x.For<INoDependency1>().Use<NoDependencyClass1>();
                x.For<INoDependency1>().Use<NoDependencyClass2>();
                x.For<INoDependency1>().Use<NoDependencyClass3>();

                x.For<IDependency1>().Use<DependencyClass1>();
            });

            var instance = container.GetInstance<IDependency1>();

            instance.GetNoDependency1().Should().BeOfType<NoDependencyClass1>();
            instance.GetNoDependency2().Should().BeOfType<NoDependencyClass2>();
            instance.GetNoDependency3().Should().BeOfType<NoDependencyClass3>();
        }


        public class DependencyClass2 : IDependency2
        {
            public DependencyClass2(INoDependency1 arg1, INoDependency1 arg2, INoDependency1 arg3)
            {
            }
        }

        public class DependencyClass1 : IDependency1
        {
            private INoDependency1 _noDependency1;
            private INoDependency1 _noDependency2;
            private INoDependency1 _noDependency3;

            public DependencyClass1(INoDependency1 NoDependencyClass1, INoDependency1 NoDependencyClass2, INoDependency1 NoDependencyClass3)
            {
                _noDependency1 = NoDependencyClass1;
                _noDependency2 = NoDependencyClass2;
                _noDependency3 = NoDependencyClass3;
            }

            public INoDependency1 GetNoDependency1()
            {
                return _noDependency1;
            }

            public INoDependency1 GetNoDependency2()
            {
                return _noDependency2;
            }

            public INoDependency1 GetNoDependency3()
            {
                return _noDependency3;
            }
        }

        public interface IDependency2
        {
        }

        public interface IDependency1
        {
            INoDependency1 GetNoDependency1();
            INoDependency1 GetNoDependency2();
            INoDependency1 GetNoDependency3();
        }

        public interface INoDependency1
        {
        }

        public class NoDependencyClass1 : INoDependency1
        {
        }

        public class NoDependencyClass2 : INoDependency1
        {
        }

        public class NoDependencyClass3 : INoDependency1
        {
        }
    }
}
