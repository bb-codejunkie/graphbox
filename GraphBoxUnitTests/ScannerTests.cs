﻿using GraphBox;
using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using GraphBox.SubConfigs;

namespace GraphBoxUnitTests
{
    public class ScannerTests
    {
        public interface IDynamicDependency1
        {
            Guid GetGuidFromDepedency1();
            Guid GetGuidFromDepedency2();
        }

        public interface IDynamicNoDependency1
        {
            Guid GetGuid();
        }

        public interface IDynamicNoDependency2
        {
            Guid GetGuid();
        }

        string source =
        @"
using System;
using GraphBoxUnitTests;

namespace Dynamics
{
    public enum SomeEnum
    {
        First,
        Second,
        Third
    }

    public class DynamicDependencyClass1 : ScannerTests.IDynamicDependency1
    {
        public delegate void TestDel();

        public static void TestImpl()
        {
        }

        TestDel test = TestImpl;
        private ScannerTests.IDynamicNoDependency1 _noDependency1;
        private ScannerTests.IDynamicNoDependency2 _noDependency2;

        public DynamicDependencyClass1(ScannerTests.IDynamicNoDependency1 noDependency1, ScannerTests.IDynamicNoDependency2 noDependency2)
        {
            _noDependency1 = noDependency1;
            _noDependency2 = noDependency2;
        }

        public Guid GetGuidFromDepedency1()
        {
            return _noDependency1.GetGuid();
        }

        public Guid GetGuidFromDepedency2()
        {
            return _noDependency2.GetGuid();
        }
    }

    public class DynamicNoDependencyClass1 : ScannerTests.IDynamicNoDependency1
    {
        public Guid GetGuid()
        {
            return new Guid(""580FF092-9683-4D0C-B4F5-26060BE6059E"");
        }
    }

    public class DynamicNoDependencyClass2 : ScannerTests.IDynamicNoDependency2
    {
        public Guid GetGuid()
        {
            return new Guid(""CE21FD4D-58F6-449B-9162-BF1FACA935CE"");
        }
    }
}";

        public ScannerTests()
        {
        }

        private Assembly GetDynamicAssembly()
        {
            var provider = new CSharpCodeProvider();
            var compilerParams = new CompilerParameters
            {
                GenerateInMemory = true,
                GenerateExecutable = false,
            };

            compilerParams.ReferencedAssemblies.Add(Assembly.GetExecutingAssembly().Location);
            var results = provider.CompileAssemblyFromSource(compilerParams, source);

            var dynamicAssambly = results.CompiledAssembly;
            return dynamicAssambly;
        }

        [Fact]
        public void Given_An_Assembly_When_Simple_Scanning_Then_An_Instance_And_Its_Dependency_Instances_Be_Resolved()
        {
            var dynamicAssembly = GetDynamicAssembly();
            var container = new Container();
            
            container.Configure(x => x.MergeConfig(new SimpleScanner(dynamicAssembly)));

            var dynamicDependency = container.GetInstance<IDynamicDependency1>();
            dynamicDependency.GetGuidFromDepedency1().Should().Be("580FF092-9683-4D0C-B4F5-26060BE6059E");
            dynamicDependency.GetGuidFromDepedency2().Should().Be("CE21FD4D-58F6-449B-9162-BF1FACA935CE");
        }
    }
}
