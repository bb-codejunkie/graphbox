﻿using GraphBox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;

namespace GraphBoxUnitTests
{
    public class IEnumerableTests
    {
        [Fact]
        public void Given_3_Types_To_An_Interface_When_Getting_IEnumerable_Then_A_Collection_Is_Returned()
        {
            var container = new Container();
            container.Configure(x => {
                x.For<INoDependency>().Use<NoDependencyClass1>();
                x.For<INoDependency>().Use<NoDependencyClass2>();
                x.For<INoDependency>().Use<NoDependencyClass3>();
            });

            var instances = container.GetInstance<IEnumerable<INoDependency>>();

            instances.Where(instance => instance.GetType() == typeof(NoDependencyClass1)).Any().Should().BeTrue();
            instances.Where(instance => instance.GetType() == typeof(NoDependencyClass2)).Any().Should().BeTrue();
            instances.Where(instance => instance.GetType() == typeof(NoDependencyClass3)).Any().Should().BeTrue();
        }

        [Fact]
        public void Given_3_Types_To_An_Interface_When_Getting_IEnumerable_From_Child_Container_Then_A_Collection_Is_Returned()
        {
            var container = new Container();
            container.Configure(x =>
            {
                x.For<INoDependency>().Use<NoDependencyClass1>();
                x.For<INoDependency>().Use<NoDependencyClass2>();
                x.For<INoDependency>().Use<NoDependencyClass3>();
            });

            var instances = container.CreateChildContainer().GetInstance<IEnumerable<INoDependency>>();

            instances.Where(instance => instance.GetType() == typeof(NoDependencyClass1)).Any().Should().BeTrue();
            instances.Where(instance => instance.GetType() == typeof(NoDependencyClass2)).Any().Should().BeTrue();
            instances.Where(instance => instance.GetType() == typeof(NoDependencyClass3)).Any().Should().BeTrue();
        }

        [Fact]
        public void Given_An_Auto_Wired_Setup_When_Getting_An_Instance_With_An_IEnumerable_Setup_Then_The_Expected_IEnumarable_Can_Extracted()
        {
            var container = new Container();
            container.Configure(x =>
            {
                x.For<HasDependenciesClass>().Use<HasDependenciesClass>();
                x.For<INoDependency>().Use<NoDependencyClass1>();
                x.For<INoDependency>().Use<NoDependencyClass2>();
                x.For<INoDependency>().Use<NoDependencyClass3>();
            });

            var hasDependenciesClass = container.GetInstance<HasDependenciesClass>();

            var instances = hasDependenciesClass.GetNoDependenciesEnumerable();
            instances.Where(instance => instance.GetType() == typeof(NoDependencyClass1)).Any().Should().BeTrue();
            instances.Where(instance => instance.GetType() == typeof(NoDependencyClass2)).Any().Should().BeTrue();
            instances.Where(instance => instance.GetType() == typeof(NoDependencyClass3)).Any().Should().BeTrue();
        }

        [Fact]
        public void Given_A_IEnumerable_Setup_When_Calling_Count_On_The_Enumerable_Then_Count_Is_Always_The_Same()
        {
            var container = new Container();
            container.Configure(x =>
            {
                x.For<HasDependenciesClass>().Use<HasDependenciesClass>();
                x.For<INoDependency>().Use<NoDependencyClass1>();
                x.For<INoDependency>().Use<NoDependencyClass2>();
                x.For<INoDependency>().Use<NoDependencyClass3>();
            });

            var hasDependenciesClass = container.GetInstance<HasDependenciesClass>();
            var count = hasDependenciesClass.GetNoDependenciesEnumerable().Count();

            hasDependenciesClass.GetNoDependenciesEnumerable().Count().Should().Be(count);
        }

        [Fact]
        public void Given_An_IEnumerable_Setup_When_Enumerating_By_Selecting_Twice_On_The_Same_Enumerable_Then_The_Same_Sequence_Is_Returned()
        {
            var container = new Container();
            container.Configure(x =>
            {
                x.For<INoDependency>().Use<NoDependencyClass1>();
                x.For<INoDependency>().Use<NoDependencyClass2>();
                x.For<INoDependency>().Use<NoDependencyClass3>();
            });
            var instances = container.GetInstance<IEnumerable<INoDependency>>();

            var Ids1 = instances.Select(x => x.GetID());
            var Ids2 = instances.Select(x => x.GetID());

            Ids1.SequenceEqual(Ids2).Should().BeTrue();
        }

        [Fact]
        public void Given_An_IEnumerable_Setup_When_Enumerating_By_Selecting_Twice_On_Different_Enumerables_Then_The_Same_Sequence_Is_Not_Returned()
        {
            var container = new Container();
            container.Configure(x =>
            {
                x.For<INoDependency>().Use<NoDependencyClass1>();
                x.For<INoDependency>().Use<NoDependencyClass2>();
                x.For<INoDependency>().Use<NoDependencyClass3>();
            });
            var instances1 = container.GetInstance<IEnumerable<INoDependency>>();
            var instances2 = container.GetInstance<IEnumerable<INoDependency>>();

            var Ids1 = instances1.Select(x => x.GetID());
            var Ids2 = instances2.Select(x => x.GetID());

            Ids1.SequenceEqual(Ids2).Should().BeFalse();
        }


        [Fact]
        public void Given_An_IEnumerable_Setup_When_Enumerating_Then_The_Instances_Should_Not_Be_Renewed()
        {
            var container = new Container();
            container.Configure(x =>
            {
                x.For<INoDependency>().Use<NoDependencyClass1>();
                x.For<INoDependency>().Use<NoDependencyClass2>();
                x.For<INoDependency>().Use<NoDependencyClass3>();
            });
            var instances = container.GetInstance<IEnumerable<INoDependency>>();
            Guid firstGuid = Guid.Empty;
            foreach(var instance in instances)
            {
                firstGuid = instance.GetID();
                break;
            }
            Guid secondGuid = Guid.Empty;

            foreach (var instance in instances)
            {
                secondGuid = instance.GetID();
                break;
            }

            firstGuid.Should().Be(secondGuid);
        }
    }

    public interface INoDependency
    {
        Guid GetID();
    }

    public class NoDependencyClass1 : INoDependency
    {
        private Guid Id = Guid.NewGuid();

        public Guid GetID()
        {
            return Id;
        }
    }

    public class NoDependencyClass2 : INoDependency
    {
        private Guid Id = Guid.NewGuid();

        public Guid GetID()
        {
            return Id;
        }
    }

    public class NoDependencyClass3 : INoDependency
    {
        private Guid Id = Guid.NewGuid();

        public Guid GetID()
        {
            return Id;
        }
    }

    public class HasDependenciesClass
    {
        IEnumerable<INoDependency> _noDependencies;

        public HasDependenciesClass(IEnumerable<INoDependency> noDependencies)
        {
            _noDependencies = noDependencies;
        }

        public IEnumerable<INoDependency> GetNoDependenciesEnumerable()
        {
            return _noDependencies;
        }
    }
}
