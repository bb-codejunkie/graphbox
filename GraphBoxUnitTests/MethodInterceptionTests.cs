﻿using FluentAssertions;
using GraphBox;
using GraphBox.GraphBoxProxy;
using System;
using Xunit;

namespace GraphBoxUnitTests
{
    public class MethodInterceptionTests
    {
        private static object _returnObject;

        [Fact]
        public void Given_A_Value_Type_As_Return_Type_When_Intercepting_Then_The_Return_Type_Can_Be_Caught()
        {
            var container = new Container();
            container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AndInterceptMethodsWith<ProxyInterceptor>());
            var instance = container.GetInstance<INoDependency1>();

            var classGuid = instance.GetClassGuid();

            ((Guid)_returnObject).Should().Be(classGuid);
        }

        [Fact]
        public void Given_A_Generic_Method_When_Method_Intercepting_Then_The_Expected_Return_Value_Is_Returned()
        {
            var container = new Container();
            container.Configure(x => x.For<INoDependency3<string>>().Use<NoDependencyClass3<string>>().AndInterceptMethodsWith<ProxyInterceptor>());
            var instance = container.GetInstance<INoDependency3<string>>();

            var convertedString = instance.ObjectToString("HelloWorld");

            convertedString.Should().Be("HelloWorld");
        }

        [Fact]
        public void Given_A_Reference_Type_As_Return_Type_When_Intercepting_Then_The_Return_Type_Can_Be_Caught()
        {
            var container = new Container();
            container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AndInterceptMethodsWith<ProxyInterceptor>());
            var instance = container.GetInstance<INoDependency1>();

            var str = instance.GetString();

            ((string)_returnObject).Should().Be(str);
        }

        [Fact]
        public void Given_A_Auto_Wired_Proxy_Setup_And_When_Intercepting_Then_The_Return_Type_Can_Be_Caught()
        {
            var container = new Container();
            container.Configure(x => 
            {
                x.For<INoDependency1>().Use<NoDependencyClass1>();
                x.For<IHasDependency1>().Use<HasDependencyClass1>().AndInterceptMethodsWith<ProxyInterceptor>();                 
            });
            var instance = container.GetInstance<IHasDependency1>();

            var str = instance.GetString();

            ((string)_returnObject).Should().Be(str);
        }

        [Fact]
        public void Given_A_Method_With_Value_And_Reference_Parameters_When_Getting_Result_The_Result_Equals_42()
        {
            var container = new Container();
            container.Configure(x =>
            {
                x.For<INoDependency2>().Use<NoDependencyClass2>().AndInterceptMethodsWith<ProxyInterceptor>();
            });
            var instance = container.GetInstance<INoDependency2>();

            var str = instance.GetResult(40, "2");

            str.Should().Be(42);
        }

        public class ProxyInterceptor : IInterceptor
        {
            public void Intercept(IInvocation invocation)
            {
                invocation.Proceed();
                _returnObject = invocation.ReturnObject;
            }
        }

        public interface INoDependency1
        {
            Guid GetClassGuid();
            string GetString();
        }

        public class NoDependencyClass1 : INoDependency1
        {
            private Guid _classGuid = Guid.NewGuid();

            public virtual Guid GetClassGuid()
            {
                return _classGuid;
            }

            public virtual string GetString()
            {
                return "Hello World";
            }
        }

        public interface IHasDependency1
        {
            Guid GetClassGuid();
            string GetString();
        }

        public class HasDependencyClass1 : IHasDependency1
        {
            private Guid _classGuid = Guid.NewGuid();
            private INoDependency1 _noDependency1;

            public HasDependencyClass1(INoDependency1 noDependency1)
            {
                _noDependency1 = noDependency1;
            }

            public virtual Guid GetClassGuid()
            {
                return _classGuid;
            }

            public virtual string GetString()
            {
                return "Hello World";
            }
        }

        public interface INoDependency2
        {
            int GetResult(int a, string b);
        }

        public class NoDependencyClass2 : INoDependency2
        {
            public virtual int GetResult(int a, string b)
            {
                return a + Convert.ToInt32(b);
            }
        }

        public interface INoDependency3<T>
        {
            T ObjectToString(object something);
        }

        public class NoDependencyClass3<T> : INoDependency3<T>
        {
            public virtual T ObjectToString(object something)
            {
                return (T) something;
            }
        }
    }
}
