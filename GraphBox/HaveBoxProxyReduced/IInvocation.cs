﻿using System.Reflection;

namespace GraphBox.GraphBoxProxy
{
    public interface IInvocation
    {
        object[] Args { get; set; }
        MethodInfo Method { get; }
        object ReturnObject { get; set; }

        void Proceed();
    }
}
