﻿
namespace GraphBox.GraphBoxProxy
{
    public interface IInterceptor
    {
        void Intercept(IInvocation invocation);
    }
}
