﻿using System;

namespace GraphBox
{
    public interface IDisposableContainer : IContainer, IDisposable
    {
        void DisposeInstance(IDisposable instance);
    }
}
