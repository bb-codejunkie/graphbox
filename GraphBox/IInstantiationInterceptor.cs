﻿
namespace GraphBox
{
    public interface IInstantiationInterceptor
    {
        void Intercept(IInstantiation Instantiation);
    }
}
