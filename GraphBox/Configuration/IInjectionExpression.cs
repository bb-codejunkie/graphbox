﻿using System;

namespace GraphBox.Configuration
{
    public interface IInjectionExpression
    {
        IInjectionProperty Use<TYPE>();
        IInjectionProperty Use(Type type);
        IInjectionProperty Use(Func<object> function);
    }
}