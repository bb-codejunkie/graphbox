﻿
namespace GraphBox.Configuration
{
    public interface ISingletonProperty : IInjectionProperty
    {
        IInjectionProperty WithPerContainerLifeTime();
    }
}
