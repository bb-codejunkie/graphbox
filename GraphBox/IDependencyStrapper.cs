﻿using GraphBox.Configuration;
using System;
using System.Collections.Generic;

namespace GraphBox
{
    internal interface IDependencyStrapper
    {
        void Strap(IDictionary<Type, IList<TypeDetails>> dependencyMap);
    }
}
